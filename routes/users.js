const express = require('express');
const router = express.Router();

router.post('/home', (req, res, next) => {
    res.send('Home');
});

module.exports = router;