import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { HttpClientModule }    from '@angular/common/http';
import { NgHttpLoaderModule } from 'ng-http-loader';
import { FormsModule } from '@angular/forms';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { AppComponent } from './app.component';
import { SearchDoctorsComponent } from './search-doctors/search-doctors.component';
import { HomepageComponent } from './homepage/homepage.component';
import { AppRoutingModule } from './app-routing.module';
import { ViewAllComponent } from './view-all/view-all.component';
import { DoctorDetailComponent } from './doctor-detail/doctor-detail.component';
import { FacebookModule } from 'ngx-facebook';
import { Ng2CarouselamosModule } from '../assets/bower_components/ng2-carouselamos';


@NgModule({
  declarations: [
    AppComponent,
    SearchDoctorsComponent,
    HomepageComponent,
    ViewAllComponent,
    DoctorDetailComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule.forRoot(),
    AngularFontAwesomeModule,
    HttpClientModule,
    FormsModule,
    NgHttpLoaderModule,
    FacebookModule.forRoot(),
    Ng2CarouselamosModule,
  ],
  providers: [Title],
  bootstrap: [AppComponent]
})
export class AppModule { }
