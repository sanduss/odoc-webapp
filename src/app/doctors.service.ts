import { Injectable } from '@angular/core';
import { Doctor, DoctorsResponse, Categories } from './models/doctor';
import { HttpClient, HttpHeaders } from '@angular/common/http';
 
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
//import { Data } from '@angular/router';
import { HttpResponse } from 'selenium-webdriver/http';
import { data } from './models/doctor';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class DoctorsService {

  //private apiUrl = 'http://localhost/demo/';  // URL to web api
  private apiUrl = 'https://ai.odoc.life/homescreen';  // URL to web api
  private postUrl = 'https://ai.odoc.life/webapp?dr=';  // URL to web api
  //private postUrl = 'http://localhost/demo/doctors.php';



  constructor(
    private http: HttpClient
  ) { }

 
// getAllDoctors (doctor: any): Observable<any>{
//     return this.http.get<any>(this.apiUrl+ 'doctors', { observe: 'response'})
//       .pipe(
//         catchError(this.handleError('getAllDoctors', []))
//       );
//   }

  getAllDoctors (): Observable<DoctorsResponse> {
        const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
        // 'Authorization': this.doctorAuthToken,
       //   'Access-Control-Allow-Origin': "http://localhost:4200/"
        })
      };
      return this.http.get<DoctorsResponse>(this.apiUrl, httpOptions)
  }

  setCategories(doctors): any {

    var outObject = doctors.reduce(function (a, e) {
      // GROUP BY estimated key (estKey), well, may be a just plain key
      // a -- Accumulator result object
      // e -- sequentally checked Element, the Element that is tested just at this itaration

      // new grouping name may be calculated, but must be based on real value of real field
      let estKey = (e['Speciality']);
      (a[estKey] ? a[estKey] : (a[estKey] = null || [])).push(e);
      return a;
    }, {});

    var categories = new Array();
    for (var element in outObject) {
      categories.push({"doctors":outObject[element], "title": element})
    }
    return categories;

  }

//   getAllDoctors () {
//     // return this.docdorsJson.Doctors
//     return this.homeDoctors
// }

  getDoctorDetails(id): Observable<any> {
      const httpOptions = {
          headers: new HttpHeaders({
              'Content-Type': 'application/json',
              'Accept': 'application/json',
             // 'Authorization': this.doctorAuthToken,
          })
      };
      return this.http.get<DoctorsResponse>(this.postUrl + id, httpOptions)
     // return this.doctorJsonDetail.data
    
  }

  getAllExplore () {
    // return this.http.get<any>(this.apiUrl + "explore")
    //   .pipe(
    //     catchError(this.handleError('getAllExplore', []))
    //   );
     return this.exploreDetails
  }

//   getDoctorDetails (doctor: any): Observable<any> {
//     return this.http.post<any>(this.postUrl, doctor, httpOptions)
//       .pipe(
//         catchError(this.handleError('getDoctorDetails', []))
//       );
//   }



    /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
 
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

 
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  private exploreDetails ={
      data:[
          {
        "image_url":"http://sgp18.siteground.asia/~whistle4/images/explore1.png",
        "display_text":"Doctors",
        "search_text":"Doctors",
          }
        
      ]
    }
  }