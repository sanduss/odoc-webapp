import { Component, OnInit, Input } from '@angular/core';

import { ViewAll } from '../view-all';

import { DoctorsService } from '../doctors.service';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

import { FacebookService, InitParams, UIParams, UIResponse } from 'ngx-facebook';

@Component({
  selector: 'app-view-all',
  templateUrl: './view-all.component.html',
  styleUrls: ['./view-all.component.css']
})
export class ViewAllComponent implements OnInit {

  closeResult: string;
  selectedDoctor: string;
  selectedDoctorUrl: string;
  currLocation: string;
  copyText: string;
  socialMessage: string;

  @Input() doctors: ViewAll;
  

  constructor(
    private doctorService: DoctorsService,
    private modalService: NgbModal,
    private fb: FacebookService
  ) {
    this.currLocation = window.location.origin;
    this.socialMessage = "";
   }

  ngOnInit() {
    this.copyText = "Copy Link";
  }

  open(content, name, url) {
    this.copyText = "Copy Link";
    this.socialMessage = "";
    this.selectedDoctor = name.Title + " " + name.FirstName + " " + name.LastName;
    this.selectedDoctorUrl = this.currLocation +"/doctor-profiles/"+ url;
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  copyInputMessage(inputElement): void {
    inputElement.select();
    document.execCommand('copy');
    inputElement.setSelectionRange(0, this.selectedDoctorUrl.length);
    this.copyText = "Copied";
  }

  resetCopyText(social:string, url: string): void {
    this.socialMessage = "";
    if(social == 'fb') {
      let initParams: InitParams = {
        appId: '200807607442279', // Add your APP ID
        xfbml: true,
        version: 'v3.0'
      };
   
      this.fb.init(initParams);
      
      let params: UIParams = {
        href: url,
        method: 'share',
        display: 'popup'
      };
     
      this.fb.ui(params)
        .then((res: UIResponse) => {
          this.socialMessage = "You've successfully shared the doctor profile.";
        })
        .catch((e: any) => {
          this.socialMessage = "";
        });
    } else if(social == 'twitter') {
      this.PopupCenter('https://twitter.com/intent/tweet?source=tweetbutton&text=I saw this great doctor\'s profile:&url='+url,'Twitter','500','300'); 
    } else if(social == 'mail') {
      window.location.href = 'mailto:?subject=Doctor referral&body=I saw this great doctor\'s profile: %0D%0A'+url;
    }  else if(social == 'whatsapp') {
      this.PopupCenter('https://wa.me/?text=I saw this great doctor\'s profile: ' + url,' Whatsapp Sharing','500','400'); 
    }

    this.copyText = "Copy Link";
  }

  PopupCenter(url, title, w, h) {
    // Fixes dual-screen position                         Most browsers      Firefox
    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : window.screenX;
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : window.screenY;

    var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

    var left = ((width / 2) - (w / 2)) + dualScreenLeft;
    var top = ((height / 2) - (h / 2)) + dualScreenTop;
    var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

    // Puts focus on the newWindow
    if (window.focus) {
        newWindow.focus();
    }
}

}
