//import { Doctor } from './doctor';
import { Title } from '@angular/platform-browser';
export class DoctorsResponse {
    //doctor: Array<any>;
    Result: String
    Message: String
    Doctors: Array<Doctor>
}

export class Categories {
    Title: String
    Subtitle: String
    Doctors: Array<Doctor>
}

export class DoctorDetail {
    isArray : Number
    data: Array<data>
}

export class data {
    
    primaryConsultantId: String
    secondaryConsultantIds: String
    title:String 
    firstName: String
    lastName: String
    type: String
    speciality: String
    practiceLocations: String
   
}

export class DoctorCategory {
    doctors: Array<Doctor>
}

export class Doctor {
    doctor: Array<any>;
    DoctorID: Number
    PrimaryId: Number
    Platform: String
    Title: String
    FirstName: String
    LastName: String
    PRO_PIC: String
    Speciality: String
    SpecialityDescription: String
    Rating: Number
    SearchTag: String
    ConsultantType: String
    TotalReview: Number
    thumbnail: String
    
}