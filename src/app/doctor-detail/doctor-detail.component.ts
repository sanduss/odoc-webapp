import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DoctorsService } from '../doctors.service';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { FacebookService, InitParams, UIParams, UIResponse } from 'ngx-facebook';
import { Meta, Title } from '@angular/platform-browser';

import { Doctor } from '../models/doctor';
import { Observable } from 'rxjs';



@Component({
  selector: 'app-doctor-detail',
  templateUrl: './doctor-detail.component.html',
  styleUrls: ['./doctor-detail.component.css']
})

export class DoctorDetailComponent implements OnInit {
 
  @Input() selectedDoctor: any;

  doctors: { "primaryConsultantId": string; "secondaryConsultantIds": string; "title": string; "firstName": string; "lastName": string; "type": string; "speciality": string; "practiceLocations": { "title": string; "address": string; }[]; "dateOfBirth": string; }[];
  global: { keyword: string; };
  showViewAll: boolean;
  hideExplore: boolean;
  showHomepage: boolean;
  DoctorDetails: any;
  doctor: any;
  // doctor = this.selectedDoctor
  noFoundData: boolean;
  closeResult: string;
  selectedDoctorStr: string;
  selectedDoctorUrl: string;
  currLocation: string;
  copyText: string;
  socialMessage: string;

  constructor(
    private route: ActivatedRoute,
    private doctorService:DoctorsService,
    private router: Router,
    private modalService: NgbModal,
    private fb: FacebookService,
    private meta: Meta,
    private titleService: Title
  ) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.currLocation = window.location.origin;
    this.socialMessage = "";

    // new change
    // this.doctor = this.selectedDoctor
   }

  ngOnInit() {
    this.hideExplore = false;
    this.noFoundData = false;
    this.showHomepage = true;
    this.showViewAll = false;

    console.log("selected doc detail: "+ this.selectedDoctor)

    this.getDoctorDetails();

    
    
    this.global = {
      keyword: ""
    }
    this.copyText = "Copy Link";
    this.socialMessage = "";
    this.setTitle( 'Doctor Detail | Page' );
    this.selectedDoctorUrl = window.location.href;
    console.log(this.selectedDoctorUrl, " === selectedDoctorUrl");
    this.noFoundData = false;
    this.doctorService.getDoctorDetails({
      doctor_id: +this.route.snapshot.paramMap.get('id'),
      row_title: this.toTitleCase(this.route.snapshot.paramMap.get('specialty').replace("_", " "))
    })
    .subscribe(doctor => {
      if(doctor == null) {
        this.noFoundData = true;
      }
      this.doctor = doctor;
      this.doctor.Rating = Math.round(this.doctor.Rating);

      this.meta.addTag({ name: 'description', property: 'og:description', content: this.doctor.Title + ' ' + this.doctor.FirstName
      + ' ' + this.doctor.LastName + ' ' + this.doctor.Speciality + ' ' + this.doctor.SpecialityDescription });
      this.meta.addTag({ name: 'url', property: 'og:url', content: this.selectedDoctorUrl });
      this.meta.addTag({ name: 'image', property: 'og:image', content: this.doctor.PRO_PIC});
      this.meta.addTag({ name: 'title', property: 'og:title', content: this.doctor.Title + ' ' + this.doctor.FirstName
      + ' ' + this.doctor.LastName });
      this.meta.addTag({ name: 'type', property: 'og:type', content: 'website'});

      this.setTitle( 'Doctor Profile | ' + this.doctor.Title + ' ' + this.doctor.FirstName
    + ' ' + this.doctor.LastName  );
    });

    this.copyText = "Copy Link";
    
  }

  public setTitle( newTitle: string) {
    this.titleService.setTitle( newTitle );
  }

  toTitleCase(str: string) : string {
    return str.replace(/\w\S*/g, function(txt){
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
  }

  open(content, name, url) {
    this.copyText = "Copy Link";
    this.socialMessage = "";
    this.selectedDoctorStr = name.Title + " " + name.FirstName + " " + name.LastName;
    this.selectedDoctorUrl = this.selectedDoctorUrl;
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  getDoctorDetails() {
    const docId = this.route.snapshot.paramMap.get('id')
    this.doctorService.getDoctorDetails(docId).subscribe(data => {
      this.selectedDoctor = data.data;
      this.doctor = data.data;
      console.log("doc detail form server: ",data)
    });
    this.doctor = this.doctorService.getDoctorDetails(docId)
     console.log(this.doctors)
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  copyInputMessage(inputElement): void {
    inputElement.select();
    document.execCommand('copy');
    inputElement.setSelectionRange(0, this.selectedDoctorUrl.length);
    this.copyText = "Copied";
  }
  
  resetCopyText(social:string, url: string): void {
    url = this.selectedDoctorUrl;
    console.log(url, " Social URL");
    this.socialMessage = "";
    if(social == 'fb') {
      let initParams: InitParams = {
        appId: '1288503561229220', // Add your APP ID
        xfbml: true,
        version: 'v3.0'
      };
   
      this.fb.init(initParams);
      
      let params: UIParams = {
        href: url,
        method: 'share',
        display: 'popup'
      };
     
      this.fb.ui(params)
        .then((res: UIResponse) => {
          this.socialMessage = "You've successfully shared the doctor .";
        })
        .catch((e: any) => {
          this.socialMessage = "";
        });
    } else if(social == 'twitter') {
      this.PopupCenter('https://twitter.com/intent/tweet?source=tweetbutton&text=I saw this great doctor\'s profile:&url='+url,'Twitter','500','300'); 
    } else if(social == 'mail') {
      window.location.href = 'mailto:?subject=Doctor referral&body=I saw this great doctor\'s profile: %0D%0A'+url;
    }  else if(social == 'whatsapp') {
      this.PopupCenter('https://wa.me/?text=I saw this great doctor\'s profile: ' + url,' Whatsapp Sharing','500','400'); 
    }

    this.copyText = "Copy Link";
  }

  goBack(): void {
    this.router.navigate(['/']);
  }

  PopupCenter(url, title, w, h) {
    // Fixes dual-screen position                         Most browsers      Firefox
    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : window.screenX;
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : window.screenY;

    var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

    var left = ((width / 2) - (w / 2)) + dualScreenLeft;
    var top = ((height / 2) - (h / 2)) + dualScreenTop;
    var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

    // Puts focus on the newWindow
    if (window.focus) {
        newWindow.focus();
    }
}
}
