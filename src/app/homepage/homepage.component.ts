import { Component, OnInit, Output } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { Router } from "@angular/router";
import { DoctorsService } from '../doctors.service';
import { Doctor, DoctorsResponse } from '../models/doctor';
import { Global } from '../models/global';
import { ViewAll } from '../view-all';

import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FacebookService, InitParams, UIParams, UIResponse } from 'ngx-facebook';

declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css'],
   
})

export class HomepageComponent implements OnInit {

  categories: Array<Doctor> = [];
  category: Array<Doctor> =[];
  //explore: Array<any> = [];
  @Output() selectingDoctor: any;
  selectedDoctors: any;
  hideExplore: boolean;
  noFoundData: boolean;
  global: Global;
  @Output() viewAllDoctor: ViewAll;
  showHomepage: boolean;
  showViewAll: boolean;
  closeResult: string;
  selectedDoctorStr: string;
  selectedDoctorUrl: string;
  currLocation: string;
  copyText: string;
  socialMessage: string;
  items: Array<any> = [];
  doctors: DoctorsResponse;
  doctorsService: any;
  image_url: any;
  display_text: any;
  search_text: any;
  index: any;
  count: number = 0; 

  constructor(
    private doctorService: DoctorsService,
    private modalService: NgbModal,
    private fb: FacebookService,
    private titleService: Title,
    private router: Router
  ) {
    this.currLocation = window.location.origin;
    this.items = [
      { name: 'assets/images/kitchen_adventurer_caramel.jpg' },
      { name: 'assets/images/kitchen_adventurer_caramel.jpg' },
      { name: 'assets/images/kitchen_adventurer_caramel.jpg' },
      { name: 'assets/images/kitchen_adventurer_caramel.jpg' },
      { name: 'assets/images/kitchen_adventurer_caramel.jpg' },
      { name: 'assets/images/kitchen_adventurer_caramel.jpg' },
    ]
  }

  ngOnInit() {
    this.hideExplore = false;
    this.noFoundData = false;
    this.showHomepage = true;
    this.showViewAll = false;
    // this.setCategories();
    this.getAllDoctors()
    console.log("Called getAllDoctors");
    this.getAllExplore();
    this.global = {
      keyword: ""
    }
    this.copyText = "Copy Link";
    this.socialMessage = "";
    this.setTitle('Doctor Profile | Home');
  }

  public setTitle(newTitle: string) {
    this.titleService.setTitle(newTitle);
  }

  redirectToDetailsView(content, item) {
    console.log("selected doctor: " + JSON.stringify(item))
    this.selectingDoctor = item
    let link = this.toLowerCaseAndChange(item.Title) + "/" + this.toLowerCaseAndChange(item.FirstName) + "_" + this.toLowerCaseAndChange(item.LastName) + "/" + item.DoctorID
    console.log(link)
    this.router.navigate([link])
  }
  
  open(content, name, url) {
    this.copyText = "Copy Link";
    this.socialMessage = "";
    this.selectedDoctorStr = name.Title + " " + name.FirstName + " " + name.LastName;
    this.selectedDoctorUrl = this.currLocation + "/doctor-profiles/" + url;
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  // angular.module('app', function ($document, $log, $scope, resizeService) {
  //   resizeService
  //     .resizeImage('resources/imageToResize', {
  //         size: 100, 
  //         sizeScale: 'ko'
  //         // Other options ...
  //     })
  //     .then(function(image){    
  //       // Add the resized image into the body
  //       var imageResized = document.createElement('img');
  //       imageResized.src = image;
  //       $document[0].querySelector('body').appendChild(imageResized);
  //     })
  //     .catch($log.error); 

  likecount( ): void{
    this.count++
    console.log("counting");
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  getAllExplore(): void {
    // this.doctorService.getAllExplore()
    //         .subscribe(
    //           explore => {
    //             this.explore = explore;
    //           }
    //         ); 
    // this.explore = this.doctorService.getAllExplore()
    //console.log(this.explore.length);
  }

  // setCategories(){
  //   this.doctorService.setCategories();
  // }

  getAllDoctors() {
    this.doctorService.getAllDoctors()
      .subscribe(
        doctors => {
          this.doctors = doctors;
          this.onSelect(this.doctors);
          this.categories = this.doctorService.setCategories(doctors.Doctors); //doctors.Doctors;
          console.log(this.categories);
        });

    // this.doctorService.getAllDoctors()
    // .subscribe(data => {
    //   // const dataa = data
    // });
    //this.categories = this.doctorsService.getAllDoctors()
    //console.log(this.categories.length)
    // console.log(this.doctors)
  }

  toLowerCaseAndChange(text) {
    return text.toLowerCase().replace(' ', '_')
  }

  onSelect(doctors: any): void {
    this.selectedDoctors = doctors;
  }

  viewAll(doctors: any): void {
    this.showHomepage = false;
    this.showViewAll = true;
    this.global = {
      keyword: doctors.title
    }
    this.viewAllDoctor = doctors;
    console.log("viewing all docs: " + doctors)
  }

  filterSearch(search: string): void {
    let serpDoctors = [];
    let matches;
    let response;
    console.log("search doc")
    this.global = {
      keyword: search
    }

    let doctors = this.doctors.Doctors;
    matches = doctors
      .filter((doctor) => {
        alert(search);
        if (doctor.SearchTag == undefined) {
          doctor.SearchTag = "";
        }
        return doctor.FirstName.indexOf(search) >= 0
          || doctor.LastName.indexOf(search) >= 0
          || doctor.SearchTag.indexOf(search) >= 0;
      })
      .map((doctor) => {
        return {
          title: doctor.Speciality,
          subtitle: doctor.SpecialityDescription,
          doctors: matches.filter((similarDoctor) => {
            return similarDoctor.Speciality == doctor.Speciality
          })
        }
      });

    if (matches.length > 0) {
      serpDoctors.push({
        title: this.doctors[this.index].title,
        subtitle: this.doctors[this.index].subtitle,
        doctors: matches.filter((doctor)=>{return this.index})
      });
    }


    for (let doctor of this.doctors.Doctors) {
      matches = this.doctors[this.index].doctors.filter(
        doctors => {
          if(doctors.SearchTag === null) {
            doctors.SearchTag = "";
          }
          return doctors.FirstName.indexOf(search) !== -1 || doctors.LastName.indexOf(search) !== -1 || doctors.SearchTag.indexOf(search) !== -1;
        });
        if( matches.length > 0) {
          serpDoctors.push( {
            title: this.doctors[this.index].title,
            subtitle: this.doctors[this.index].subtitle,
            doctors: matches
          });
        }

      }

    response = {
      doctors: matches,
      keyword: search
    }

   // this.updateDoctorList(response);
  }

  updateDoctorList(response: any) {

    if (response.keyword === "") {
      this.getAllDoctors();
      this.getAllExplore();
      this.hideExplore = false;
      this.showHomepage = true;
      this.showViewAll = false;
    } else {
      this.hideExplore = true;
    }
    // console.log("updating: "+ JSON.stringify(response))
    if (response.doctors.length > 0) {
      this.noFoundData = false;
    } else {
      this.noFoundData = true;
    }
    this.categories = response.doctors;
  }

  copyInputMessage(inputElement): void {
    inputElement.select();
    document.execCommand('copy');
    inputElement.setSelectionRange(0, this.selectedDoctorUrl.length);
    this.copyText = "Copied";
  }

  resetCopyText(social: string, url: string): void {
    console.log(url, " Social URL");
    this.socialMessage = "";
    if (social == 'fb') {
      let initParams: InitParams = {
        appId: '1288503561229220', // Add your APP ID
        xfbml: true,
        version: 'v3.0'
      };

      this.fb.init(initParams);

      let params: UIParams = {
        href: url,
        method: 'share',
        display: 'popup'
      };

      this.fb.ui(params)
        .then((res: UIResponse) => {
          this.socialMessage = "You've successfully shared the doctor profile.";
        })
        .catch((e: any) => {
          this.socialMessage = "";
        });
    } else if (social == 'twitter') {
      this.PopupCenter('https://twitter.com/intent/tweet?source=tweetbutton&text=I saw this great doctor\'s profile:&url=' + url, 'Twitter', '500', '300');
    } else if (social == 'mail') {
      window.location.href = 'mailto:?subject=Doctor referral&body=I saw this great doctor\'s profile: %0D%0A' + url;
    } else if (social == 'whatsapp') {
      this.PopupCenter('https://wa.me/?text=I saw this great doctor\'s profile: ' + url, ' Whatsapp Sharing', '500', '400');
    }

    this.copyText = "Copy Link";
  }

  PopupCenter(url, title, w, h) {
    // Fixes dual-screen position                         Most browsers      Firefox
    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : window.screenX;
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : window.screenY;

    var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

    var left = ((width / 2) - (w / 2)) + dualScreenLeft;
    var top = ((height / 2) - (h / 2)) + dualScreenTop;
    var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

    // Puts focus on the newWindow
    if (window.focus) {
      newWindow.focus();
    }
  }

}
