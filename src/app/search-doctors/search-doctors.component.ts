import { HomepageComponent } from './../homepage/homepage.component';
import { Component, OnInit, Input, EventEmitter, Output, PipeTransform } from '@angular/core';
import { Doctor } from '../models/doctor';
import { Global } from '../models/global';
import {Router} from "@angular/router";
//import { getHeapSpaceStatistics } from 'v8';


declare var $: any;

@Component({
  selector: 'app-search-doctors',
  templateUrl: './search-doctors.component.html',
  styleUrls: ['./search-doctors.component.css']
})
export class SearchDoctorsComponent implements OnInit {

  @Input() doctors: Doctor;
  @Input() global: Global;
  @Output() getDoctorsSerp: EventEmitter<any> = new EventEmitter<any>();
  serpDoctors = [];
  selected: Doctor;
  hideSearch: boolean;
  index: any;
  

  constructor(private router: Router) {
    this.hideSearch = false;
   }

  ngOnInit() {
    this.global = {
      keyword: ""
    }
    let toHideSearch = document.getElementById("for-mobile");
    if(toHideSearch !== null) {
      var style = window.getComputedStyle(toHideSearch);
      if(style.display == 'block') {
        $('.container-wrapper').css('margin-top', '0px');
        this.hideSearch = true;
      }  else {
        this.hideSearch = false;
      }
    }
  }

  // refreshAll(stringval: string): void {
  //   if(stringval == ""){
  //       //call the gethomescreen function
  //       alert("should call");
  //   }
  // }

  goBack(): void {
    this.router.navigate(['/']);
  }

  clearSearch(): void {
    let response;

    this.global = {
      keyword: ""
    }

    response = {
      doctors:  this.doctors,
      keyword: ""
    }

    this.getDoctorsSerp.emit(response);
  }

  onEnter(event: any, searchval : string) : void { 
    //alert("test");
    
    let search = searchval.toLowerCase();
    this.serpDoctors = [];
    let matches;
    let response;
    console.log("searching..");
    for (let doctor in this.doctors["Doctors"]) {
        matches = this.doctors["Doctors"].filter(
        doctors => {
          if(doctors.SearchTag === null) {
          doctors.SearchTag = "";
          }
         return doctors.FirstName.toLowerCase().indexOf(search) !== -1 || doctors.LastName.toLowerCase().indexOf(search) !== -1 || doctors.SearchTag.toLowerCase().indexOf(search) !== -1;
        });
        if( matches.length > 0) {
          this.serpDoctors.push( {
            title: this.doctors["Doctors"].title,
            subtitle: this.doctors["Doctors"].subtitle,
            doctors: matches
          });
        }
    }
    response = {
      doctors: this.serpDoctors,
       keyword: search
    }
    console.log(response);
    this.getDoctorsSerp.emit(response);
    
  }
}


